import React from "react";
import { NavLink } from "react-router-dom";
const Header = () => {
  return (
    <header style={{ textAlign: "center" }}>
      <div>Todo Title</div>
      <NavLink to="/">Home</NavLink>
      {" | "}
      <NavLink to="/about">About</NavLink>
    </header>
  );
};

export default Header;
