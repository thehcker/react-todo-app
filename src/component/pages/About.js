import React, { Component } from "react";

class About extends Component {
  render() {
    return (
      <div>
        <h2>About Me</h2>
        <p>Hello, I am FullStack Web Developer!</p>
      </div>
    );
  }
}
export default About;
