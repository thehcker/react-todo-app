import React, { Component } from "react";

class AddTodo extends Component {
  state = {
    title: ""
  };
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.addTodo(this.state.title);
    this.setState({
      title: ""
    });
  };
  render() {
    return (
      <form method="POST" style={StyleAddTodo} onSubmit={this.handleSubmit}>
        <input
          type="text"
          name="title"
          placeholder="Add Todo"
          value={this.state.title}
          onChange={this.handleChange}
        />
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
const StyleAddTodo = {
  paddingLeft: "30px"
};
export default AddTodo;
