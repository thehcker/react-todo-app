import React, { Component } from "react";
import TodoItems from "./TodoItems";

export default class Todos extends Component {
  render() {
    return (
      <div>
        <TodoItems
          todo={this.props.todo}
          handleChange={this.props.handleChange}
          delTodo={this.props.delTodo}
        />
      </div>
    );
  }
}
