import React, { Component } from "react";

export default class TodoItems extends Component {
  getStyle = () => {
    return {
      listStyle: "none",
      textDecoration: this.props.todo.completed ? "line-through" : "none"
    };
  };
  render() {
    const { id, title } = this.props.todo;
    return (
      <div>
        <ul>
          <li key={id} style={this.getStyle()}>
            <input
              type="checkbox"
              onChange={this.props.handleChange.bind(this, id)}
            />
            {title}
            <button style={btn} onClick={this.props.delTodo.bind(this, id)}>
              x
            </button>
          </li>
        </ul>
      </div>
    );
  }
}
const btn = {
  margin: "10px",
  borderRadius: "10px",
  background: "#ff0000",
  border: " 0px"
};
