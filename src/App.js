import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Todos from "./component/Todos";
import About from "./component/pages/About";
import "./css/main.css";
import AddTodo from "./component/AddTodo";
import Header from "./component/pages/Header";
import cuid from "cuid";

export default class App extends Component {
  state = {
    todos: [
      { id: cuid(), title: "Introduction to HTML", completed: false },
      { id: cuid(), title: "Introduction to Css", completed: false },
      { id: cuid(), title: "Introduction to Js", completed: false }
    ]
  };
  handleChange = id => {
    this.setState({
      todos: this.state.todos.map(todo => {
        console.log(todo.id);
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    });
  };
  delTodo = id => {
    this.setState({
      todos: [...this.state.todos.filter(todo => todo.id !== id)]
    });
  };

  addTodo = title => {
    const newTodo = {
      id: cuid(),
      title: title,
      completed: false
    };
    this.setState({
      todos: [...this.state.todos, newTodo]
    });
  };

  render() {
    return (
      <Router>
        <div
          style={{
            backgroundColor: "#ccc",
            padding: 30
          }}
        >
          <h1 style={{ paddingLeft: "40px" }}>Todo Items</h1>
          <Header />
          <Route
            exact
            path="/"
            render={props => (
              <>
                <AddTodo todos={this.state.todos} addTodo={this.addTodo} />
                {this.state.todos.map(todo => (
                  <Todos
                    todo={todo}
                    handleChange={this.handleChange}
                    delTodo={this.delTodo}
                  />
                ))}
              </>
            )}
          />
          <Route path="/about" component={About} />
        </div>
      </Router>
    );
  }
}
